#ifndef SISTEMADEMENSAJES_H
#define SISTEMADEMENSAJES_H

#include "ConexionJugador.h"
#include "Proxy.h"
#include "Proxy2.h"
#include <string>
#include <vector>

#if EJ == 4 || EJ == 5
#include "Proxy.h"
#elif EJ == 6
#include "Proxy2.h"
#endif

using namespace std;

class SistemaDeMensajes {
  public:
    SistemaDeMensajes();//1
    ~SistemaDeMensajes();//2
    // Pre: 0 <= id < 4
    void registrarJugador(int id, string ip);//1
    // Pre: 0 <= id < 4
    bool registrado(int id) const;//1

    // Pre: registrado(id)
    void enviarMensaje(int id, string mensaje);//1

    // Pre: registrado(id)
    string ipJugador(int id) const;//1
    // Pre: registrado(id)
    void desregistrarJugador(int id);//3

    Proxy* obtenerProxy(int id);//4



  private:
    ConexionJugador* _conns[4];
    //vector<*Proxy> = vector<Proxy*>
    vector<Proxy*> _pro;

    ConexionJugador** _conn[4];
};

#endif
