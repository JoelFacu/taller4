#include "SistemaDeMensajes.h"
#include "Proxy.h"
#include "Proxy2.h"

SistemaDeMensajes::SistemaDeMensajes() :_conns(){

}
//Constructor por defecto: T::T()




bool SistemaDeMensajes::registrado(int id)const{
    return _conns[id] != NULL;
}
/* Pre: 0 <= id < 4

1)_conns[id] aca hablamos de la coneccion del jugador

 2)el  NULL es para indicar que un puntero tiene
la direccion 0. Sin embargo, esto es un mero reemplazo sintatico.

 3)con esto digo que la coneccion del jugador tiene dirreciones y ninguna es vacia

 4)TEST(SistemaDeMensajes, constructor) {
  SistemaDeMensajes sdm;
  ASSERT_FALSE(sdm.registrado(0));
  ASSERT_FALSE(sdm.registrado(1));
  ASSERT_FALSE(sdm.registrado(2));
  ASSERT_FALSE(sdm.registrado(3));
}

 */


void SistemaDeMensajes::registrarJugador(int id, string ip) {


    if (registrado(id)) {
        delete _conns[id];
    }
    ConexionJugador *jugador = new ConexionJugador(ip);
    _conns[id] = jugador;
}
//Conns es un array de 4 elementos
//Id es un indice del arrray
//E ip es la conexión a la que tenes que apuntar

//x cada array ,tengo que hacer una nueva coneccion
//Y cuando sobre escribir un array
//Eliminas la anterior para no perder memoria

    /*registrar un ip
    nuevo para un ip ya registrado. Para ello es necesario crear una instancia nueva de ConexionJugador
    de forma de sobreescribir la original

     para que si el jugador ya está registrado, lo actualice con el ip parámetro.



 */



void SistemaDeMensajes::enviarMensaje(int id, string mensaje) {
    _conns[id]->enviarMensaje(mensaje);
}




/*
 1)_conns[id].enviarMensaje(string) = _conns[id]->enviarMensaje()
  aca coloco la ID y veo que mensaje sale


 TEST(SistemaDeMensajes, enviar_mensaje) {
  Internet::internet().limpiar();
  //
  SistemaDeMensajes sdm;
  sdm.registrarJugador(0, "IP1");
  sdm.enviarMensaje(0, "A");
  //
  ASSERT_EQ(Internet::internet().mensajes(), list<Msg>({{"IP1", "A"}}));
  sdm.enviarMensaje(0, "B");
  ASSERT_EQ(Internet::internet().mensajes(), list<Msg>({{"IP1", "A"}, {"IP1", "B"}}));
}



 */



string SistemaDeMensajes::ipJugador(int id)const{
    return _conns[id]->ip();
}
/*
  1)aca coloco la ID y veo que ip sale


*/

SistemaDeMensajes::~SistemaDeMensajes() {
    delete _conns[0];
    delete _conns[1];
    delete _conns[2];
    delete _conns[3];
    //De ser necesario, implementar el destructor.
    //para ver que no pierda memoria o ,cuestion de orga2
    //agregar en sistemademensaje.h


    /*
     * TEST(SistemaDeMensajes, destructor) {
         SistemaDeMensajes sdm;
        }
     */

    for (Proxy *p : _pro) {//ejemplo en pdf de  contrusctores y lista de incializacion
        delete p;
    }
}
/////////////////////////3

void SistemaDeMensajes::desregistrarJugador(int id) {
    delete _conns[id]; //borro lo de antes
     _conns[id] = NULL;//aseguro mi valor


} //eliminar la instancia de ConexionJugador. Además, queremos que se pueda registrar un ip
  //nuevo para un ip ya registrado. Para ello es necesario crear una instancia nueva de ConexionJugador
  //de forma de sobreescribir la origina
  //
////////////////////////4

Proxy* SistemaDeMensajes::obtenerProxy(int id){

    Proxy* jugador = new Proxy(_conns[id]);
    _pro.push_back(jugador);
    return jugador;

}
//x cada jugador hay un nuevo proxy
//que dado un id de jugador cree un proxy para este jugador y devuelva un puntero al mismo.
/*
 *
 *
#if EJ != 6
Proxy *SistemaDeMensajes::obtenerProxy(int id) {
    Proxy* conJugador = new Proxy(_conns[id]);
    this->_proxys.push_back(conJugador);
    return  conJugador;
}
#else
Proxy *SistemaDeMensajes::obtenerProxy(int id) {
    Proxy* conJugador = new Proxy(&(this->_conns[id]));
    this->_proxys.push_back(conJugador);
    return  conJugador;
}
#endif*/